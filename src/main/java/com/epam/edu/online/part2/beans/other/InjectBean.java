package com.epam.edu.online.part2.beans.other;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InjectBean {

    @Autowired
    private MyOther primary;
    @Qualifier("otherBeanB")
    @Autowired
    private MyOther myOther1;
    @Qualifier("otherBeanC")
    @Autowired
    private MyOther myOther2;

    @Autowired
    private List<MyOther> myOthers;

    private final OtherBeanA otherBeanA;
    private OtherBeanB otherBeanB;
    @Autowired
    @Qualifier("beanC")
    private OtherBeanC otherBeanC;

    public InjectBean(OtherBeanA otherBeanA) {
        this.otherBeanA = otherBeanA;
    }
    @Autowired
    public void setOtherBeanB(OtherBeanB otherBeanB) {
        this.otherBeanB = otherBeanB;
    }

    public List<MyOther> getMyOthers() {
        return myOthers;
    }

    public String primaryExample(){
        return "primary = " + primary +
                ", qualifier1 = " + myOther1 +
                ", qualifier2 = " + myOther2;
    }

    @Override
    public String toString() {
        return "InjectBean{" +
                "otherBeanA=" + otherBeanA.hashCode() +
                ", otherBeanB=" + otherBeanB.hashCode() +
                ", otherBeanC=" + otherBeanC.hashCode() +
                '}';
    }
}
