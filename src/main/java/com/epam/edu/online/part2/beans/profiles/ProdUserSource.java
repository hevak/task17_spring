package com.epam.edu.online.part2.beans.profiles;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan
@Profile("prod")
public class ProdUserSource {
    @Bean
    public User dataSource() {
        return new User("admin", "gfhjkm");
     }

}
