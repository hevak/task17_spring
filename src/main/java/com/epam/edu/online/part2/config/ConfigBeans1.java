package com.epam.edu.online.part2.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.epam.edu.online.part2.beans.bean1")
public class ConfigBeans1 {
}
