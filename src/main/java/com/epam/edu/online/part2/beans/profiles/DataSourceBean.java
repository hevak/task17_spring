package com.epam.edu.online.part2.beans.profiles;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan
public class DataSourceBean {
    @Bean
    @Profile("dev")
    public Datasource getDataSourceForDevelopment() {
        return new Datasource("localhost:8080", "postgres", "postgres", "testDb");
    }

    @Bean
    @Profile("prod")
    public Datasource getDataSourceForProduction() {
        return new Datasource("127.0.0.1:8080", "postgres", "postgres", "prodDb");
    }

}
