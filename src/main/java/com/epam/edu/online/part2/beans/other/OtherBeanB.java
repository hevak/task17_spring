package com.epam.edu.online.part2.beans.other;

import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Scope("prototype")
public class OtherBeanB implements MyOther{
    @Override
    public String name() {
        return "OtherBeanB";
    }
}

