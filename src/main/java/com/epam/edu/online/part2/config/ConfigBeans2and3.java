package com.epam.edu.online.part2.config;

import com.epam.edu.online.part2.beans.bean3.BeanE;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "com.epam.edu.online.part2.beans.bean2")
@ComponentScan(basePackages = "com.epam.edu.online.part2.beans.bean3",
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = BeanE.class))
public class ConfigBeans2and3 {
}
