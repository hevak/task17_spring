package com.epam.edu.online.part2.beans.profiles;

public class Datasource {
    private String url;
    private String username;
    private String password;
    private String nameDb;

    public Datasource(String url, String username, String password, String nameDb) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.nameDb = nameDb;
    }

    @Override
    public String toString() {
        return "DataSource{" +
                "url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", nameDb='" + nameDb + '\'' +
                '}';
    }
}

