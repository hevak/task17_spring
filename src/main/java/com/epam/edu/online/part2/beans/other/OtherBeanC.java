package com.epam.edu.online.part2.beans.other;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("beanC")
@Order(Ordered.HIGHEST_PRECEDENCE)
@Scope("prototype")
public class OtherBeanC implements MyOther{
    @Override
    public String name() {
        return "OtherBeanC";
    }
}
