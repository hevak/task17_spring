package com.epam.edu.online.part2;

import com.epam.edu.online.part2.beans.other.InjectBean;
import com.epam.edu.online.part2.beans.other.OtherBeanA;
import com.epam.edu.online.part2.beans.other.OtherBeanB;
import com.epam.edu.online.part2.beans.other.OtherBeanC;
import com.epam.edu.online.part2.beans.profiles.DataSourceBean;
import com.epam.edu.online.part2.beans.profiles.Datasource;
import com.epam.edu.online.part2.beans.profiles.ProdUserSource;
import com.epam.edu.online.part2.beans.profiles.User;
import com.epam.edu.online.part2.config.ConfigBeans1;
import com.epam.edu.online.part2.config.ConfigBeans2and3;
import com.epam.edu.online.part2.config.ConfigOtherBeans;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;

public class Application {
    private static final Logger log = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "prod");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                ConfigBeans1.class,
                ConfigBeans2and3.class,
                ConfigOtherBeans.class,
                // component
                ProdUserSource.class,
                // bean
                DataSourceBean.class
        );
        // display all beans
        for (String name : context.getBeanDefinitionNames()) {
            log.info(name);
        }
        InjectBean injectBean = (InjectBean) context.getBean("injectBean");
        // display all beans created by constructor, setter and field(reflection)
        log.info(injectBean.toString());
        // order
        injectBean.getMyOthers().forEach(log::info);
        // primary
        log.info(injectBean.primaryExample());
        // scope
        log.info(context.getBean(OtherBeanA.class).hashCode());// singleton
        log.info(context.getBean(OtherBeanA.class).hashCode());// singleton
        log.info(context.getBean(OtherBeanB.class).hashCode());// prototype
        log.info(context.getBean(OtherBeanB.class).hashCode());// prototype
        log.info(context.getBean(OtherBeanC.class).hashCode());// prototype
        log.info(context.getBean(OtherBeanC.class).hashCode());// prototype
        // profile
        log.info(context.getBean(Datasource.class));
        log.info(context.getBean(User.class));

    }
}
