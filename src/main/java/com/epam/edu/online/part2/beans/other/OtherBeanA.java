package com.epam.edu.online.part2.beans.other;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = 5)
@Primary
@Scope("singleton")
public class OtherBeanA implements MyOther{
    @Override
    public String name() {
        return "OtherBeanA";
    }
}
