package com.epam.edu.online.part1.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanD implements BeanValidator {
    private static final Logger log = LogManager.getLogger(BeanD.class);
    private String name;
    private int value;


    public BeanD() {
    }

    public BeanD(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public void validate() {
        if (this.name == null) {
            throw new IllegalArgumentException("name == null");
        }
        if (this.value < 0) {
            throw new IllegalArgumentException("value < 0");
        }
    }
    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
    private void initD() {
        log.info("initD");
    }
    private void destroyD() {
        log.info("destroyD");
    }
}
