package com.epam.edu.online.part1;

import com.epam.edu.online.part1.config.ConfigBeanAll;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class Application {
    private static final Logger log = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ConfigBeanAll.class);
        Arrays.stream(context.getBeanDefinitionNames()).forEach(log::info);
    }
}
