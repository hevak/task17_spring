package com.epam.edu.online.part1.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    private static final Logger log = LogManager.getLogger(MyBeanFactoryPostProcessor.class);
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        for (String name : beanFactory.getBeanDefinitionNames()) {
            if (name.equals("beanB")) {
                BeanDefinition beanDefinition = beanFactory.getBeanDefinition(name);
                log.info(name + " - init method - " + beanDefinition.getInitMethodName());
                beanDefinition.setInitMethodName("newInitB");
                log.info(name + " - init method - " + beanDefinition.getInitMethodName());
            }
        }
    }
}
