package com.epam.edu.online.part1.config;

import com.epam.edu.online.part1.beans.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;

@Configuration
@Import(ConfigBeanBCD.class)
public class ConfigBeanAll {
    private String nameB;
    private String nameC;
    private int valueC;
    private int valueD;

    public ConfigBeanAll() {
    }
    @Bean
    public boolean setProperties(ConfigBeanBCD configBeanBCD) {
        nameB = configBeanBCD.getBeanB().getName();
        nameC = configBeanBCD.getBeanC().getName();
        valueC = configBeanBCD.getBeanC().getValue();
        valueD = configBeanBCD.getBeanD().getValue();
        return true;
    }
    @Bean
    public Validator validator(){
        return new Validator();
    }
    @Bean
    public MyBeanFactoryPostProcessor beanFactoryPostProcessor(){
        return new MyBeanFactoryPostProcessor();
    }
    @Bean
    public BeanA getBeanAbc() {
        return new BeanA(nameB, valueC);
    }
    @Bean
    public BeanA getBeanAbd() {
        return new BeanA(nameB, valueD);
    }
    @Bean
    public BeanA getBeanAcd() {
        return new BeanA(nameC, valueD);
    }

    @Bean
    public BeanE getBeanEabc(@Qualifier("getBeanAbc") BeanA beanA) {
        return new BeanE(beanA.getName(), beanA.getValue());
    }
    @Bean
    public BeanE getBeanEbd(@Qualifier("getBeanAbd") BeanA beanA) {
        return new BeanE(beanA.getName(), beanA.getValue());
    }
    @Bean
    public BeanE getBeanEcd(@Qualifier("getBeanAcd") BeanA beanA) {
        return new BeanE(beanA.getName(), beanA.getValue());
    }

    @Bean
    @Lazy
    public BeanF getBeanF() {
        return new BeanF();
    }
}
