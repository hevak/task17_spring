package com.epam.edu.online.part1.beans;

public interface BeanValidator {
    void validate();
}
