package com.epam.edu.online.part1.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    private static final Logger log = LogManager.getLogger(BeanA.class);
    private String name;
    private int value;

    public BeanA() {
    }

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public void validate() throws IllegalArgumentException{
        if (this.name == null) {
            throw new IllegalArgumentException("name == null");
        }
        if (this.value < 0) {
            throw new IllegalArgumentException("value < 0");
        }
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public void destroy() {
        log.info("destroy");
    }

    @Override
    public void afterPropertiesSet() {
        log.info("afterPropertiesSet");
    }
}
