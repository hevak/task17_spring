package com.epam.edu.online.part1.beans;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {
    private static final Logger log = LogManager.getLogger(BeanE.class);
    private String name;
    private int value;

    public BeanE() {
    }

    public BeanE(String name, int value) {
        this.name = name;
        this.value = value;
    }

    @PostConstruct
    public void postConstruct() {
        log.info("@PostConstruct");
    }
    @PreDestroy
    public void preDestroy(){
        log.info("@PreDestroy");
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public void validate() {
        if (this.name == null) {
            throw new IllegalArgumentException("name == null");
        }
        if (this.value < 0) {
            throw new IllegalArgumentException("value < 0");
        }
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
