package com.epam.edu.online.part1.config;

import com.epam.edu.online.part1.beans.BeanB;
import com.epam.edu.online.part1.beans.BeanC;
import com.epam.edu.online.part1.beans.BeanD;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("application.properties")
public class ConfigBeanBCD {

    @Value("${B.name}")
    private String nameB;
    @Value("${B.value}")
    private int valueB;
    @Value("${C.name}")
    private String nameC;
    @Value("${C.value}")
    private int valueC;
    @Value("${D.name}")
    private String nameD;
    @Value("${D.value}")
    private int valueD;

    @Bean(name = "beanB", initMethod = "initB", destroyMethod = "destroyB")
    public BeanB getBeanB() {
        return new BeanB(nameB, valueB);
    }

    @Bean(name = "beanC", initMethod = "initC", destroyMethod = "destroyC")
    @DependsOn(value ={"beanB", "beanD"})
    public BeanC getBeanC() {
        return new BeanC(nameC, valueC);
    }

    @Bean(name = "beanD", initMethod = "initD", destroyMethod = "destroyD")
    public BeanD getBeanD() {
        return new BeanD(nameD, valueD);
    }
}
