package com.epam.edu.online.part1.beans;

public class BeanF implements BeanValidator {
    private String name;
    private int value;

    public BeanF() {
    }

    public BeanF(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public void validate() {
        if (this.name == null) {
            throw new IllegalArgumentException("name == null");
        }
        if (this.value < 0) {
            throw new IllegalArgumentException("value < 0");
        }
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
